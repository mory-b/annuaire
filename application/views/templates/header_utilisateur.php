<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.blue_grey-blue.min.css">
		<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
		<script   src="https://code.jquery.com/jquery-3.4.1.min.js"   integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="   crossorigin="anonymous"></script>
		<title>Annuaire</title>
	</head>
	<body>
		<div class="mdl-layout mdl-js-layout">
		  <header class="mdl-layout__header mdl-layout__header--scroll">
		    <div class="mdl-layout__header-row">
		      <span class="mdl-layout-title">Globalis</span>
		      <!-- Add spacer, to align navigation to the right -->
		      <div class="mdl-layout-spacer"></div>
		      <!-- Navigation -->
		      <nav class="mdl-navigation">
		      	<a class="mdl-navigation__link" href='<?php echo site_url("connexion/deconnexion") ?>'>
							<?php echo $identite ?> <b> <?php echo $role ?> </b> (Se déconnecter)
						</a>
		      </nav>
		    </div>
		  </header>
		  <div class="mdl-layout__drawer">
		    <span class="mdl-layout-title">Globalis</span>
		    <nav class="mdl-navigation">
					<a class="mdl-navigation__link" href='<?php echo site_url("connexion/deconnexion") ?>'>
						<div>
							<?php echo $identite ?>
							<br>
							<b><?php echo $role ?></b>
							<br>
							(Se déconnecter)
						</div>
					</a>
		    </nav>
		  </div>
		  <main class="mdl-layout__content">
		    <div class="page-content"><!-- Your content goes here -->
