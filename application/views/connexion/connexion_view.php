<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.blue_grey-blue.min.css">
		<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
		<script   src="https://code.jquery.com/jquery-3.4.1.min.js"   integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="   crossorigin="anonymous"></script>
		<title>Annuaire</title>
	</head>
	<body>
		<h4 style="text-align: center">Projet Annuaire</h4>
<div class="mdl-grid">

	<div class="mdl-cell mdl-cell--2-col">
	</div>
	<div class="mdl-cell mdl-cell--10-col">
		
		<h4>Identifiez-vous</h4>
		<div>
			Bienvenue sur l'annuaire GLOBALIS.
			<br>
			Saisir votre email et votre mot de passe
		</div>

		<div class="mdl-grid" >
			<div class="mdl-cell mdl-cell--12-col">

				<div class="mdl-cell mdl-cell--6-col" style="color:red">
					<?php
					if(isset($message_erreur)){
						echo $message_erreur;
					}
						?>
				</div>

				<?php
				echo validation_errors();

					echo form_open('connexion');
				?>
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--8-col">
					<input class="mdl-textfield__input" type="text" id="email" name="email" value="<?php echo set_value('email',isset($utilisateur['email']) ? $utilisateur['email'] :'') ?>" autocomplete="off">
					<label class="mdl-textfield__label" for="email">Votre email*</label>
					<span class="mdl-textfield__error">email incorect</span>
				</div>
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--8-col">
					<input class="mdl-textfield__input" type="password" id="mdp" name="mdp"  value="<?php echo set_value('libelle',isset($role['libelle']) ? $role['libelle'] :'') ?>" autocomplete="off">
					<label class="mdl-textfield__label" for="mot de passe">Votre mot de passe *</label>
					<span class="mdl-textfield__error">Uniquement les lettres et les espaces sont acceptés</span>
				</div>
							
				<br>
				<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent" value="connexion" />

				</form>
				<br>
				* : Les champs précédés de ce signe sont obligatoires
			</div>
		</div>
	</div>
</div>
<footer style="padding: 15px">
	<hr style="width: 100%">
	<center>©2018 - GLOBALIS</center>
</footer>
</body>
</html>
