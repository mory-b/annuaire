<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--2-col">
	</div>
	<div class="mdl-cell mdl-cell--10-col">
		<h4> Utilisateur </h4>

		<h5> Filtrer les utilisateurs </h5>

		<div class="mdl-grid">
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--5-col">
				<input class="mdl-textfield__input" type="text" id="nom" name="nom"  autocomplete="off">
				<label class="mdl-textfield__label" for="nom">Nom</label>
				<span class="mdl-textfield__error">Uniquement les lettres et les espaces sont acceptés</span>
			</div>
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--5-col">
				<input class="mdl-textfield__input" type="text" id="prenom" name="prenom"  autocomplete="off">
				<label class="mdl-textfield__label" for="prenom">Prenom</label>
				<span class="mdl-textfield__error">Uniquement les lettres et les espaces sont acceptés</span>
			</div>
		</div>

		<div class="mdl-grid">
			<hr class="mdl-cell mdl-cell--7-col">
			<div class="mdl-cell mdl-cell--3-col">
				Filtre avancé
				<button id="filtre_avance" class="mdl-button mdl-js-button mdl-button--icon"> 
					<i class="material-icons">keyboard_arrow_down</i>
				</button>
			</div>
		</div>

		<div class="mdl-grid" id="block_filtre_avance" style="display: none">
			<div class="mdl-cell mdl-cell--5-col">
				<table>
					<tr><td>Afficher les utilisateurs actifs</td></tr>
					<tr>
						<td>
							<input type="radio"  id="oui" name="actif" value="oui" checked>
							<span>Oui</span>
						</td>
						<td>
							<input type="radio"  id="non" name="actif"value="non">
							<span>Non</span>
						</td>
					</tr>
				</table>
			</div>
			<div class="mdl-cell mdl-cell--6-col">
				<label for="role"> Rôle </label>
				<select name="role" id="role">
					<option value='tous'>Tous les rôles</option>
					<?php

						foreach ($roles as $key => $value) {
							echo '<option value='.$key.'>'.$value.'</option>';
						}

					?>

				</select>
			</div>
		</div>

		<div>
			<div class="mdl-layout-spacer"></div>
			<button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" id="filtrer">
			  Filtrer
			</button>

			<button class="mdl-button mdl-js-button mdl-js-ripple-effect" id="reinitialiser">
			  Réinitialiser les filtres
			</button>
		</div>


		<br>

		<a href='<?php echo site_url("utilisateur/creation") ?>' class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">
       	Créer un utilisateur
     	</a>

		<table class="mdl-data-table mdl-js-data-table mdl-cell mdl-cell--10-col" style="margin-top: 30px">

			<thread>
				<tr>
					<th class="mdl-data-table__cell--non-numeric" style="text-align: center" id ="trie_nom" >Nom</th>
					<th class="mdl-data-table__cell--non-numeric" style="text-align: center" id ="trie_prenom">Prénom</th>
          			<th class="mdl-data-table__cell--non-numeric" style="text-align: center" id ="trie_role">Rôle</th>
          			<th class="mdl-data-table__cell--non-numeric" style="text-align: center"></th>
					<th class="mdl-data-table__cell--non-numeric" style="text-align: center"></th>
          			<th class="mdl-data-table__cell--non-numeric" style="text-align: center"></th>
				</tr>
			</thread>
			<tbody id="liste_utilisateur">

			</tbody>

		</table>

		<br>

		<div class="mdl-grid">
			<div id="pagination" class="mdl-cell mdl-cell--3-col"></div>
			<div class="mdl-cell mdl-cell--3-col" id="nombre_resultat"></div>
		</div>
	</div>
</div>

<script type="text/javascript">

	var trie_nom = 1;
	var trie_prenom = 0;
	var trie_role = 0;

	$('#filtre_avance').click(function(){
		$("#block_filtre_avance").fadeToggle("slow","swing");
	});

	$("#pagination").on('click','a',function(e){
		e.preventDefault();
		let page = $(this).attr('data-ci-pagination-page');
		chargementPage(page);
	});

	chargementPage(0);

	function chargementPage(page){

		let parametre={};

		parametre.actif = ($('input[name=actif]:checked').val() == "oui")  ? 1 : 0;

		if($('input[name=nom]').val() !=="") parametre.nom = $('input[name=nom]').val();
		if($('input[name=prenom]').val() !=="") parametre.prenom = $('input[name=prenom]').val();
		if($('select[name=role]').val() !=="") parametre.role = $('select[name=role]').val();
		parametre.trie_nom = trie_nom;
		parametre.trie_prenom = trie_prenom;
		parametre.trie_role = trie_role;

		$.ajax({
			type:'get',
			url: "<?php echo site_url() ?>/utilisateur/recuperationListeUtilisateur/"+page,
			data: parametre,
			success: function(data){
				data = JSON.parse(data)
				$("#pagination").html(data.pagination);
				$("#liste_utilisateur").empty();
				for(item in data.items){
					remplissageTbody(data.items[item])
				}
				$("#nombre_resultat").empty();
				$("#nombre_resultat").append('Nombres de résultats : ' + data.nb_items);
			}
		});
	}

	$('#filtrer').click(function(){
		chargementPage(0);
	});

	$('#reinitialiser').click(function(){
		$('input[name=nom]').val("");
		$('input[name=prenom]').val("");
		$('#oui').prop('checked',true);
		$('select[name=role]').val("tous");
		chargementPage(0);
	});

	$('#trie_nom').click(function(){
		trie_nom = trie_nom == 1 ? 0 : 1;
		if(trie_nom == 1){
			trie_role = 0;
			trie_prenom= 0;
		}
		chargementPage(0);
	});

	$('#trie_prenom').click(function(){
		trie_prenom = trie_prenom == 1 ? 0 : 1;
		if(trie_prenom == 1){
			trie_role = 0;
			trie_nom= 0;
		}
		chargementPage(0);
	});

	$('#trie_role').click(function(){
		trie_role = trie_role == 1 ? 0 : 1;
		if(trie_role == 1){
			trie_prenom = 0;
			trie_nom= 0;
		}
		chargementPage(0);
	});

	$('#liste_utilisateur').on('click','.archiver',function(){

		if(confirm("Voulez vous archiver cette utilisateur ?")){
			$.ajax(
			{
				url:"<?php echo site_url() ?>/utilisateur/archiver/" + $(this).data('id'),
				type: 'POST'
			});

			let page = $("#pagination").find('strong').text();
			chargementPage(page);
		}

	});

	$('#liste_utilisateur').on('click','.actif',function(){

		let etat_activation = $(this).data('activation') == 1 ? "désactiver" : "activer";
		if(confirm("Voulez vous "+ etat_activation +" cette utilisateur ?")){
		$.ajax(
			{
				url:"<?php echo site_url() ?>/utilisateur/activer/" + $(this).data('id'),
				type: 'POST'
			}
		);

		let page = $("#pagination").find('strong').text();
		chargementPage(page);
		}

	});

	function remplissageTbody(utilisateur){
	
		let archive_color = (utilisateur.archive == 0) ? "black": "grey";
		let actif_color = (utilisateur.actif == 1) ? "black": "grey";

		let utilisateur_row = '<tr>';
		utilisateur_row += '<td style="text-align: center">'+ utilisateur.nom +'</td>';
		utilisateur_row += '<td style="text-align: center">'+ utilisateur.prenom +'</td>';
	   	utilisateur_row += '<td style="text-align: center">'+ utilisateur.libelle +'</td>';
		utilisateur_row += '<td style="text-align: center">';
		utilisateur_row += '<a href="<?php echo site_url('utilisateur/modification/') ?>'+ utilisateur.id +'">';
		utilisateur_row += '<button class="mdl-button mdl-js-button mdl-button--icon">';
		utilisateur_row += '<i class="material-icons">edit</i>';
		utilisateur_row += '</button>';
		utilisateur_row += '</a>';
		utilisateur_row += '</td>';
		utilisateur_row += '<td style="text-align: center">';
		utilisateur_row += '<button class="mdl-button mdl-js-button mdl-button--icon archiver" data-id="'+ utilisateur.id +'" >';
		utilisateur_row += '<i class="material-icons" style=" color :'+  archive_color +'">restore_from_trash</i>';
		utilisateur_row += '</button>';
		utilisateur_row += '</td>';
		utilisateur_row += '<td style="text-align: center">';
		utilisateur_row += '<button class="mdl-button mdl-js-button mdl-button--icon actif" data-id="'+ utilisateur.id +'" data-activation="'+ utilisateur.actif +'">';
		utilisateur_row += '<i class="material-icons" style=" color :'+  actif_color +'">tag_faces</i>';
		utilisateur_row += '</button>';
		utilisateur_row += '</td>';
		utilisateur_row += "</tr>"

		$('#liste_utilisateur').append(utilisateur_row);

	}

</script>
