	<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--2-col">
	</div>
	<div class="mdl-cell mdl-cell--10-col">
		<h4>Utilisateur</h4>

		<br>

		<?php

		$status = "";

		 if(strcmp($title, 'ajouter' ) === 0){
		 	$status = "Créer";
		 }elseif(strcmp($title, 'modifier' ) === 0){
		 	$status = "Modifier";
		 }

		 echo "<h3> $status un utilisateur </h3>"

		 ?>



		<br>

		* : Les champs précédés de ce signe sont obligatoires

		<h3>Information générales</h3>

		<br>

		<div style="color: red">
			<?php 
				if(isset($erreur_identifiant)) { 
					echo $erreur_identifiant;
				} 
			?>
				
			</div>

		<h4>Général</h4>

		<?php
			echo validation_errors();

			if(strcmp($title, 'ajouter' ) === 0){
				echo form_open('utilisateur/creation');
			}elseif(strcmp($title, 'modifier' ) === 0){
				echo form_open('utilisateur/modification/'.$id);
			}
		?>

			<div class="mdl-grid">

				<div class="mdl-cell mdl-cell--6-col">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input" type="text" id="nom" name="nom" value="<?php echo set_value('nom',isset($utilisateur['nom']) ? $utilisateur['nom'] :'') ?>" autocomplete="off">
						<label class="mdl-textfield__label" for="nom">Nom *</label>
						<span class="mdl-textfield__error">Uniquement les lettres et les espaces sont acceptés</span>
					</div>
				</div>

				<div class="mdl-cell mdl-cell--6-col">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input" type="text" id="prenom" name="prenom" value="<?php echo set_value('prenom',isset($utilisateur['prenom']) ? $utilisateur['prenom'] :'') ?>" autocomplete="off">
						<label class="mdl-textfield__label" for="prenom">Prénom(s)*</label>
						<span class="mdl-textfield__error">Uniquement les lettres et les espaces sont acceptés</span>
					</div>
				</div>

				<div class="mdl-cell mdl-cell--6-col">
					<label for="role"> Fonction(s) * </label>
					<select name="role" id="role">

						<?php
						foreach ($roles as $key ) {
							if($key['archive'] == 0){
								if($key['id'] == $utilisateur['role']){
									echo '<option value='.$key['id'].' selected>'.$key['libelle'].'</option>';
								}else{
									echo '<option value='.$key['id'].'>'.$key['libelle'].'</option>';
								}
							}
						}
						?>
					
					</select>
				</div>

				<?php if(!isset($utilisateur['identifiant'])):?>
				<div class="mdl-cell mdl-cell--6-col">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input" type="text" id="identifiant" name="identifiant" value="<?php echo set_value('identifiant','') ?>" autocomplete="off">
						<label class="mdl-textfield__label" for="identifiant">identifiant*</label>
						<span class="mdl-textfield__error">Uniquement les lettres et les espaces sont acceptés</span>
					</div>
				</div>
			<?php endif; ?>

				<div class="mdl-cell mdl-cell--6-col">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input" type="text" id="email" name="email"  value="<?php echo set_value('email',isset($utilisateur['email']) ? $utilisateur['email'] :'') ?>" autocomplete="off">
						<label class="mdl-textfield__label" for="email">Email</label>
						<span class="mdl-textfield__error">email incorect</span>
					</div>
				</div>

				<div class="mdl-cell mdl-cell--6-col">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input" type="password" id="mdp" name="mdp">
						<label class="mdl-textfield__label" for="mdp">mot de passe*</label>
						<span class="mdl-textfield__error">Uniquement les lettres et les espaces sont acceptés</span>
					</div>
				</div>

			</div>


			<?php
			if (strcmp($status,'Créer') == 0){
				echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent" value="Ajouter" />';
			}elseif (strcmp($title, 'modifier' ) === 0){
				echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent" value="Modifier" />';
			}
			?>


		</form>
	</div>
</div>
