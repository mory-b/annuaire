<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--2-col">
	</div>
	<div class="mdl-cell mdl-cell--10-col">
		<h4> Fonctions </h4>

		<h5> Filtrer les fonctions </h5>


		<div class="mdl-grid" id="block_filtre_avance">

					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--5-col">
						<input class="mdl-textfield__input" type="text" id="libelle" name="libelle" autocomplete="off">
						<label class="mdl-textfield__label" for="libelle">Libelle de la fonction</label>
						<span class="mdl-textfield__error">Uniquement les lettres et les espaces sont acceptés</span>
					</div>

					<div class="mdl-cell mdl-cell--5-col">
						<table>
							<tr><td>Afficher les libellés archivés</td></tr>
							<tr>
								<td>
										<input type="radio"  id="oui" name="archivage" value="oui" checked>
										<span>Oui</span>
								</td>
								<td>
										<input type="radio"  id="non" name="archivage"value="non">
										<span>Non</span>
								</td>
							</tr>
						</table>
					</div>

					<div class="mdl-cell mdl-cell--5-col">
						<div class="mdl-layout-spacer"></div>
						<button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" id="filtrer">
						  Filtrer
						</button>

						<button class="mdl-button mdl-js-button mdl-js-ripple-effect" id="reinitialiser">
						  Réinitialiser les filtres
						</button>
					</div>
		</div>

		<a href='<?php echo site_url("fonction/creation") ?>' class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent"> Créer une fonction </a>

		<table class="mdl-data-table mdl-js-data-table mdl-cell mdl-cell--8-col" style="margin-top: 30px">

			<thread>
				<tr>
					<th class="mdl-data-table__cell--non-numeric" style="text-align: center" id="trie_libelle">Libéllé</th>
					<th class="mdl-data-table__cell--non-numeric" style="text-align: center"></th>
					<th class="mdl-data-table__cell--non-numeric" style="text-align: center"></th>
				</tr>
			</thread>
			<tbody id="liste_role"></tbody>

		</table>

		<br>

		<div class="mdl-grid">
			<div id="pagination" class="mdl-cell mdl-cell--3-col"></div>
			<div class="mdl-cell mdl-cell--3-col" id="nombre_resultat"></div>
		</div>
	</div>

</div>

<script type="text/javascript">

	var trie_libelle = 1;

	$("#pagination").on('click','a',function(e){
		e.preventDefault();
		let page = $(this).attr('data-ci-pagination-page');
		chargementPage(page);
	});

	chargementPage(0);



	function chargementPage(page){

		let parametre={};

		parametre.archive = ($('input[name=archivage]:checked').val() == "oui")  ? 0 : 1;
		if($('input[name=libelle]').val() !==""){
			parametre.libelle = $('input[name=libelle]').val();
		}
		parametre.trie_libelle = trie_libelle;

		$.ajax({
			type:'get',
			url: "<?php echo site_url() ?>/roles/recuperationListeRole/"+page,
			data: parametre,
			success: function(data){
					data = JSON.parse(data)
					$("#pagination").html(data.pagination);
					$("#liste_role").empty();
					for(item in data.items){
						remplissageTbody(data.items[item], data.page)
					}
					$("#nombre_resultat").empty();
					$("#nombre_resultat").append('Nombres de résultats : ' + data.nb_items);
				}
			}
		);
	}	

	$('#filtrer').click(function(){
	chargementPage(0);
	});

	$('#reinitialiser').click(function(){
		$('input[name=libelle]').val("");
		$('#oui').prop('checked',true);
		chargementPage(0);
	});

	$('#trie_libelle').click(function(){
		trie_libelle = trie_libelle == 1 ? 0 : 1;
		chargementPage(0);
	});

	$('#liste_role').on('click','.archiver',function(){
		if(confirm("Voulez vous archiver ou désarchiver cette fonction ?")){
			$.ajax(
				{
					url:"<?php echo site_url() ?>/fonction/archiver/" + $(this).data('id'),
					type: 'POST'
				}
			);
			chargementPage($(this).data('page'));
		}
	});

	function remplissageTbody(role, page){

			let archive_color = (role.archive == 0) ? "black": "grey";
			let role_row = '<tr>';

			role_row += '<td style="text-align: center">'+ role.libelle +'</td>';
			role_row += '<td style="text-align: center">';
			role_row += '<a href="<?php echo site_url('fonction/modification/') ?>'+ role.id +'">';
			role_row += '<button class="mdl-button mdl-js-button mdl-button--icon">';
			role_row += '<i class="material-icons">edit</i>';
			role_row += '</button>';
			role_row += '</a>';
			role_row += '</td>';
			role_row += '<td style="text-align: center">';
			role_row += '<button class="mdl-button mdl-js-button mdl-button--icon archiver" data-id="'+ role.id +'" data-page="'+ page +'">';
			role_row += '<i class="material-icons" style=" color :'+  archive_color +'">restore_from_trash</i>';
			role_row += '</button>';
			role_row += '</td>';
			role_row += "</tr>"

			$('#liste_role').append(role_row);

	}

</script>
