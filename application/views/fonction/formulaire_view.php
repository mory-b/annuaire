<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--2-col">
	</div>
	<div class="mdl-cell mdl-cell--10-col">

		<h4>Fonctions</h4>

		<br>

		<?php

			$status = "";

			if(strcmp($title, 'ajouter' ) === 0){
				$status = "Créer";
			}elseif(strcmp($title, 'modifier' ) === 0){
				$status = "Modifier";
			}

			echo "<h3> $status une fonction </h3>";

		 ?>

		<br>

		* : Les champs précédés de ce signe sont obligatoires

		<h3>Information générales</h3>

		<br>

		<div class="mdl-grid" >
			<div class="mdl-cell mdl-cell--12-col">

				<?php
				echo validation_errors();

				if(strcmp($title, 'ajouter' ) === 0){
					echo form_open('fonction/creation');
				}elseif(strcmp($title, 'modifier' ) === 0){
					echo form_open('fonction/modification/'.$id);
				}
				?>
							
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--6-col">
						<input class="mdl-textfield__input" type="text" id="libelle" name="libelle" value="<?php echo set_value('libelle',isset($le_role['libelle']) ? $le_role['libelle'] :'') ?>" autocomplete="off">
						<label class="mdl-textfield__label" for="libelle">Libelle de la fonction *</label>
						<span class="mdl-textfield__error">Uniquement les lettres et les espaces sont acceptés</span>
					</div>

					<?php

					if (strcmp($status,'Créer') == 0){
						echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent" value="Ajouter" />';
					}elseif (strcmp($title, 'modifier' ) === 0){
						echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent" value="Modifier" />';
					}

					?>

				</form>
			</div>
		</div>
	</div>
</div>
