<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--2-col">
	</div>
	<div class="mdl-cell mdl-cell--10-col">
		<h4> Annuaire </h4>

		<h5> Filtrer les contacts de l'annuaire </h5>

		<div class="mdl-grid">
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--5-col">
				<input class="mdl-textfield__input" type="text" id="nom" name="nom">
				<label class="mdl-textfield__label" for="nom">Nom</label>
				<span class="mdl-textfield__error">Uniquement les lettres et les espaces sont acceptés</span>
			</div>
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--5-col">
				<input class="mdl-textfield__input" type="text" id="prenom" name="prenom">
				<label class="mdl-textfield__label" for="prenom">Prenom</label>
				<span class="mdl-textfield__error">Uniquement les lettres et les espaces sont acceptés</span>
			</div>
		</div>

		<div class="mdl-grid">
			<hr class="mdl-cell mdl-cell--7-col">
			<div class="mdl-cell mdl-cell--3-col">
				Filtre avancé
				<button id="filtre_avance"> v </button>
			</div>

		</div>

		<div class="mdl-grid" id="block_filtre_avance" style="display: none">
			<div class="mdl-cell mdl-cell--5-col">
				<table>
					<tr><td>Afficher les libellés archivés</td></tr>
					<tr>
						<td>
							<input type="radio"  id="oui" name="archivage" value="oui" checked>
							<span>Oui</span>
						</td>
						<td>
							<input type="radio"  id="non" name="archivage"value="non">
							<span>Non</span>
						</td>
					</tr>
				</table>
			</div>
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--5-col">
				<input class="mdl-textfield__input" type="text" id="telephone" name="telephone">
				<label class="mdl-textfield__label" for="telephone">Téléphone</label>
			</div>
		</div>

		<div>
			<div class="mdl-layout-spacer"></div>
			<button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" id="filtrer">
			  Filtrer
			</button>

			<button class="mdl-button mdl-js-button mdl-js-ripple-effect" id="reinitialiser">
			  Réinitialiser les filtres
			</button>
		</div>


		<br>

		<?php if($role == "administrateur"): ?>
		<a href='<?php echo site_url("annuaire/creation") ?>' class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent"> Créer un contact</a>
		<?php endif; ?>

		<table class="mdl-data-table mdl-js-data-table mdl-cell mdl-cell--10-col" style="margin-top: 30px">

			<thread>
				<tr>
					<th class="mdl-data-table__cell--non-numeric" style="text-align: center"  id="trie_societe">Société</th>
					<th class="mdl-data-table__cell--non-numeric" style="text-align: center"  id="trie_nom">Nom</th>
					<th class="mdl-data-table__cell--non-numeric" style="text-align: center"  id="trie_prenom">Prénom</th>
					<th class="mdl-data-table__cell--non-numeric" style="text-align: center"  id="trie_telephone">Téléphone</th>
					<?php if($role == "administrateur"): ?>
					<th class="mdl-data-table__cell--non-numeric" style="text-align: center">detail</th>
					<th class="mdl-data-table__cell--non-numeric" style="text-align: center">archiver</th>
					<?php endif; ?>
				</tr>
			</thread>
			<tbody id="liste_contact">

			</tbody>

		</table>

		<br>

		<div class="mdl-grid">
			<div id="pagination" class="mdl-cell mdl-cell--3-col"></div>
			<div class="mdl-cell mdl-cell--3-col" id="nombre_resultat"></div>
		</div>
	</div>
</div>

<script type="text/javascript">

	var trie_societe = 1;
	var trie_nom = 0;
	var trie_prenom = 0;
	var trie_telephone = 0;

	$('#filtre_avance').click(function(){

		$("#block_filtre_avance").fadeToggle("slow","swing");

	});

	$("#pagination").on('click','a',function(e){
		e.preventDefault();
		let page = $(this).attr('data-ci-pagination-page');
		chargementPage(page);
	});

	chargementPage(0);

	function chargementPage(page){

		let parametre={};

		parametre.archive = ($('input[name=archivage]:checked').val() == "oui")  ? 0 : 1;
		parametre.trie_nom = trie_nom;
		parametre.trie_prenom = trie_prenom;
		parametre.trie_telephone = trie_telephone;
		parametre.trie_societe = trie_societe;


		if($('input[name=nom]').val() !=="") parametre.nom = $('input[name=nom]').val();
		if($('input[name=prenom]').val() !=="") parametre.prenom = $('input[name=prenom]').val();
		if($('input[name=telephone]').val() !=="") parametre.telephone = $('input[name=telephone]').val();

		$.ajax({
			type:'get',
			url: "<?php echo site_url() ?>/annuaire/recuperationListeContact/"+page,
			data: parametre,
			success: function(data){
				data = JSON.parse(data);
				$("#pagination").html(data.pagination);
				$("#liste_contact").empty();
				for(item in data.items){
					remplissageTbody(data.items[item], data.societes, data.status)
				}
				$("#nombre_resultat").empty();
				$("#nombre_resultat").append('Nombres de résultats : ' + data.nb_items);
			}
		});
	}

	$('#filtrer').click(function(){

		chargementPage(0);

	});

	$('#reinitialiser').click(function(){

		$('input[name=nom]').val("");
		$('input[name=prenom]').val("");
		$('input[name=telephone]').val("");
		$('#oui').prop('checked',true);
		chargementPage(0);

	});

	$('#trie_prenom').click(function(){
		trie_prenom = trie_prenom == 1 ? 0 : 1;
		if(trie_prenom == 1){
			trie_societe = 0;
			trie_nom= 0;
			trie_telephone=0;
		}
		chargementPage(0);
	});

	$('#trie_nom').click(function(){
		trie_nom = trie_nom == 1 ? 0 : 1;
		if(trie_nom == 1){
			trie_societe = 0;
			trie_prenom= 0;
			trie_telephone=0;
		}
		chargementPage(0);
	});

	$('#trie_telephone').click(function(){
		trie_telephone = trie_telephone == 1 ? 0 : 1;
		if(trie_telephone == 1){
			trie_societe = 0;
			trie_nom= 0;
			trie_telephone=0;
		}
		chargementPage(0);
	});

	$('#trie_societe').click(function(){
		trie_societe = trie_societe == 1 ? 0 : 1;
		if(trie_societe == 1){
			trie_prenom = 0;
			trie_nom= 0;
			trie_telephone=0;
		}
		chargementPage(0);
	});

	$('#liste_contact').on('click','.archiver',function(){
		if(confirm("Voulez vous archiver ou désarchiver ce contact ?")){
			$.ajax(
				{
					url:"<?php echo site_url() ?>/annuaire/archiver/" + $(this).data('id'),
					type: 'POST'
				}
			);

			let page = $("#pagination").find('strong').text();
			chargementPage(page);
		}
	});

	function remplissageTbody(contact, societes, status){

		let archive_color = (contact.archive == 0) ? "black": "grey";
		
		let contact_row = '<tr>';
		contact_row += '<td style="text-align: center">'+ contact.libelle +'</td>';
		contact_row += '<td style="text-align: center">'+ contact.nom +'</td>';
		contact_row += '<td style="text-align: center">'+ contact.prenom +'</td>';
		contact_row += '<td style="text-align: center">'+ contact.telephone_fixe +'</td>';
		if(status == "administrateur"){
			contact_row += '<td style="text-align: center">';
			contact_row += '<a href="<?php echo site_url('annuaire/detail/') ?>'+ contact.id +'">';
			contact_row += '<button class="mdl-button mdl-js-button mdl-button--icon">';
			contact_row += '<i class="material-icons">search</i>';
			contact_row += '</button>';
			contact_row += '</a>';
			contact_row += '</td>';
			contact_row += '<td style="text-align: center">';
			contact_row += '<button class="mdl-button mdl-js-button mdl-button--icon archiver" data-id="'+ contact.id +'">';
			contact_row += '<i class="material-icons" style=" color :'+  archive_color +'">restore_from_trash</i>';
			contact_row += '</button>';
			contact_row += '</td>';
		}
		contact_row += "</tr>"

		$('#liste_contact').append(contact_row);

	}

</script>
