<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--2-col">
	</div>
	<div class="mdl-cell mdl-cell--10-col">
		<h4>Annuaire</h4>

		<br>

		<?php

		$status = "";

		 if(strcmp($title, 'ajouter' ) === 0){
		 	$status = "Créer";
		 }elseif(strcmp($title, 'modifier' ) === 0){
		 	$status = "Modifier";
		 }

		 echo "<h3> $status un contact </h3>"

		 ?>

		<br>

		* : Les champs précédés de ce signe sont obligatoires

		<h3>Information générales</h3>

		<br>


		<h4>Général</h4>

		<?php
			echo validation_errors();

			if(strcmp($title, 'ajouter' ) === 0){
				echo form_open_multipart('annuaire/creation');
			}elseif(strcmp($title, 'modifier' ) === 0){
				echo form_open_multipart('annuaire/modification/'.$id);
			}
		?>

			<div class="mdl-grid">

				<div class="mdl-cell mdl-cell--6-col">
					<table>
						<tr><td>Civilité *</td></tr>
						<tr>
							<?php foreach ($civilites as $key => $value) {
								echo '<td>';
								echo '<label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="'.$value['libelle'].'">';
								echo '<span class="mdl-radio__label"> '.$value['libelle'].' </span>';
								if(isset($contact['civilite'] )){
									if($contact['civilite'] == $value['id']){
										echo '<input type="radio" class="mdl-radio__button" id="'.$value['libelle'].'" name="civilite" value="'.$value['id'].'" checked>';
									}else{
										 echo '<input type="radio" class="mdl-radio__button" id="'.$value['libelle'].'" name="civilite" value="'.$value['id'].'">';
									}
								}else{
									echo '<input type="radio" class="mdl-radio__button" id="'.$value['libelle'].'" name="civilite" value="'.$value['id'].'">';
								}
								echo '</label>';
								echo '</td>';
							}?>
						</tr>
					</table>
				</div>
				<div class="mdl-cell mdl-cell--6-col">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input" type="text" id="nom" name="nom" value="<?php echo set_value('nom',isset($contact['nom']) ? $contact['nom'] :'') ?>" autocomplete="off">
						<label class="mdl-textfield__label" for="nom">Nom *</label>
						<span class="mdl-textfield__error">Uniquement les lettres et les espaces sont acceptés</span>
					</div>
				</div>

				<div class="mdl-cell mdl-cell--6-col">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input" type="text" id="prenom" name="prenom" value="<?php echo set_value('prenom',isset($contact['prenom']) ? $contact['prenom'] :'') ?>" autocomplete="off">
						<label class="mdl-textfield__label" for="prenom">Prénom(s)*</label>
						<span class="mdl-textfield__error">Uniquement les lettres et les espaces sont acceptés</span>
					</div>
				</div>
				<div class="mdl-cell mdl-cell--6-col">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input" type="text" id="telephone" name="telephone_fixe"  value="<?php echo set_value('telephone_fixe',isset($contact['telephone_fixe']) ? $contact['telephone_fixe'] :'') ?>" autocomplete="off">
						<label class="mdl-textfield__label" for="telephone">Téléphone fixe</label>
						<span class="mdl-textfield__error">Ce champs doit commencer par 0, contenir 10 chiffres et être valide</span>
					</div>
				</div>

				<div class="mdl-cell mdl-cell--6-col">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input" type="text" id="ddn" name="ddn" value="<?php echo set_value('ddn',isset($contact['ddn']) && $contact['ddn'] != '0000-00-00' ? date_format(new DateTime($contact['ddn']),'d/m/Y') :'') ?>" autocomplete="off">
						<label class="mdl-textfield__label" for="ddn">Date de naissance</label>
						<span class="mdl-textfield__error">jj/mm/aaaa</span>
					</div>
				</div>
				<div class="mdl-cell mdl-cell--6-col">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input" type="text" id="telephone_mobile" name="telephone_mobile"  value="<?php echo set_value('telephone_mobile',isset($contact['telephone_mobile']) ? $contact['telephone_mobile'] :'') ?>" autocomplete="off">
						<label class="mdl-textfield__label" for="telephone_mobile">Téléphone mobile</label>
						<span class="mdl-textfield__error">Ce champs doit commencer par 06 ou 07 et contenir 10 chiffres</span>
					</div>
				</div>

				<div class="mdl-cell mdl-cell--6-col">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input" type="text" id="email" name="email" value="<?php echo set_value('email',isset($contact['email']) ? $contact['email'] :'') ?>" autocomplete="off">
						<label class="mdl-textfield__label" for="email">Email</label>
						<span class="mdl-textfield__error">email incorect</span>
					</div>
				</div>

			</div>

		<h4>Employeur</h4>

			<div class="mdl-grid">

				<div class="mdl-cell mdl-cell--6-col">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input" type="text" id="employeur" name="employeur" value="<?php echo set_value('employeur',isset($societe['libelle']) ? $societe['libelle'] :'') ?>" >
						<label class="mdl-textfield__label" for="employeur">Société *</label>
						<span class="mdl-textfield__error">Uniquement les lettres et les espaces sont acceptés</span>
					</div>
				</div>

				<div class="mdl-cell mdl-cell--6-col">
					<label for="fonction"> Fonction(s) * </label>
					<select name="fonction" id="fonction">

						<?php
						foreach ($fonctions as $key ) {
							if($key['id'] == $contact['fonction']){
								echo '<option value='.$key['id'].' selected>'.$key['libelle'].'</option>';
							}else{
								echo '<option value='.$key['id'].'>'.$key['libelle'].'</option>';
							}
						}
						?>

					</select>
				</div>

			</div>

		<h4>Divers</h4>

			<div class="mdl-grid">
				<?php if(!isset($contact['lien_photo'])):?>
					<div class="mdl-cell mdl-cell--6-col">
						<label for="photo">photo</label>
						<input type="file" name="photo" id="photo" size="20" />
					</div> 
				<?php endif;?>

				<div class="mdl-cell mdl-cell--6-col">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					    <textarea class="mdl-textfield__input" type="text" rows= "3" id="commentaires" name="commentaires">
								<?php echo set_value('commentaires',isset($contact['commentaire']) ? $contact['commentaire'] :'') ?>
							</textarea>
					    <label class="mdl-textfield__label" for="commentaires">Commentaires</label>
					</div>
				</div>

			</div>

			<?php
			if (strcmp($status,'Créer') == 0){
				echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent" value="Ajouter" />';
			}elseif (strcmp($title, 'modifier' ) === 0){
				echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent" value="Modifier" />';
			}
			?>


		</form>
	</div>
</div>
