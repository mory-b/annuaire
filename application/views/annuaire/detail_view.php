<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--2-col">
	</div>
	<div class="mdl-cell mdl-cell--10-col">
		<h6>Annuaire</h6>
		<h5> <?php echo $contact['nom'].' '. $contact['prenom'] ?> </h5>

		<br>

		<div class="mdl-grid">
			<div class="mdl-cell mdl-cell--6-col">Général</div>
			<div class="mdl-cell mdl-cell--6-col">
				<a href="<?php echo site_url("annuaire/modification").'/'.$id ?> "> 
					<i class="material-icons">edit</i>
				</a>
			</div>
		</div>

		<div class="mdl-grid">

			<div class="mdl-cell mdl-cell--6-col">Civilité</div>
			<div class="mdl-cell mdl-cell--6-col">Nom</div>

			<div class="mdl-cell mdl-cell--6-col">
				<?php echo $civilite['libelle']; ?>
			</div>

			<div class="mdl-cell mdl-cell--6-col"> <?php echo $contact['nom'] ?> </div>

			<div class="mdl-cell mdl-cell--6-col">Prénom</div>
			<div class="mdl-cell mdl-cell--6-col">Téléphone</div>

			<div class="mdl-cell mdl-cell--6-col"> <?php echo $contact['prenom'] ?> </div>
			<div class="mdl-cell mdl-cell--6-col"> <?php echo $contact['telephone_fixe'] ?> </div>

			<div class="mdl-cell mdl-cell--6-col">Date de naissance</div>
			<div class="mdl-cell mdl-cell--6-col">Téléphone mobile</div>

			<div class="mdl-cell mdl-cell--6-col"> 
				<?php 
					if ($contact['ddn'] != '0000-00-00' && $contact['ddn'] != NULL){
						echo date_format(new DateTime($contact['ddn']),'d/m/Y');
					}
				?> 
			</div>
			<div class="mdl-cell mdl-cell--6-col"> <?php echo $contact['telephone_mobile'] ?> </div>

			<div class="mdl-cell mdl-cell--6-col">Email</div>
			
			<div class="mdl-cell mdl-cell--6-col"> <?php echo $contact['email'] ?> </div>
			
		</div>

		<br>

		<div>Détail</div>
		<div class="mdl-grid">

			<div class="mdl-cell mdl-cell--6-col">Société</div>
			<div class="mdl-cell mdl-cell--6-col">Fonction(s)</div>

			<div class="mdl-cell mdl-cell--6-col">
				<?php echo $societe['libelle'];	?>
			</div>
			<div class="mdl-cell mdl-cell--6-col"> 
				<?php echo $fonction['libelle'];?>
			</div>
			
		</div>

		<br>

		<div>Divers</div>
		<div class="mdl-grid">

			<div class="mdl-cell mdl-cell--6-col">Photo</div>
			<div class="mdl-cell mdl-cell--6-col">Commentaire</div>

			<div class="mdl-cell mdl-cell--6-col"></div>
			<div class="mdl-cell mdl-cell--6-col"> <?php echo $contact['commentaire'] ?> </div>
			
		</div>

	</div>
</div>