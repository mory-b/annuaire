	<?php

class Utilisateur_model extends CI_Model{

	public function __construct(){

		$this->load->database();

	}

	public function fetch_utilisateur($page, $nbItemsParPage, $actif, $role, $nom, $prenom,$trie_nom, $trie_prenom, $trie_role)
	{
		$query;
		$queryNbItems;
		$parametre=[];

		$this->db->select('u.id, u.nom, u.prenom, u.actif, r.libelle');
		$this->db->from('utilisateur u');

		$this->db->join('roles r', 'r.id = u.role','inner');
		$this->db->where(array('r.archive' => 0));

		if($actif == 0){
			$this->db->where(array('u.actif' => 0));
		}

		if($role != 'tous'){
			$this->db->where('u.role', $role);
		}

		if(!empty($nom)){
			$this->db->like('u.nom', $nom);
		}

		if(!empty($prenom)){
			$this->db->like('u.prenom', $prenom);
		}

		if($trie_nom == 1){
			$this->db->order_by('u.nom', 'ASC');
		}

		if($trie_prenom == 1){
			$this->db->order_by('u.prenom', 'ASC');
		}

		if($trie_role == 1){
			$this->db->order_by('r.libelle', 'ASC');
		}

		$this->db->limit($nbItemsParPage, $page);

		$query = $this->db->get();

		//------------------------------//

		$this->db->select('u.id, u.nom, u.prenom, u.actif, r.libelle');
		$this->db->from('utilisateur u');

		$this->db->join('roles r', 'r.id = u.role','inner');
		$this->db->where(array('r.archive' => 0));
	
		
		if($actif == 0){
			$this->db->where(array('u.actif' => 0));
		}

		if($role != 'tous'){
			$this->db->where('u.role', $role);
		}

		if(!empty($nom)){
			$this->db->like('u.nom', $nom);
		}

		if(!empty($prenom)){
			$this->db->like('u.prenom', $prenom);
		}

		$queryNbItems = $this->db->get();

		return array($query->result_array(),$queryNbItems->num_rows());
	}


	public function get_utilisateurs()
	{

			$query = $this->db->get('utilisateur');
			return $query->result_array();

	}

	public function insert_utilisateur($identifiant, $nom, $prenom, $role, $email, $mdp){

		$query = $this->db->get_where('utilisateur', array('identifiant' => $identifiant));

		$utilisateur = $query->row();

		if($query->num_rows() > 0){
			return false;
		}


		$data = array(
				'nom' => $nom,
				'prenom' => $prenom,
				'role' => $role,
				'identifiant' => $identifiant,
				'email' => $email,
				'mdp' => $mdp,
				'archive' => 0,
				'actif' => 1
		);


		$this->db->insert('utilisateur', $data);
		return true;

	}

	public function update_utilisateur($id, $nom, $prenom, $role, $email, $mdp){

		$data = array(
			'nom' => $nom,
			'prenom' => $prenom,
			'role' => $role,
			'email' => $email,
		);

		if(!empty($mdp)){
			$data['mdp'] = $mdp;
		}

		$this->db->where('id', $id);
		$this->db->update('utilisateur', $data);

	}

	public function get_utilisateur($id){

		$query = $this->db->get_where('utilisateur', array('id' => $id));
		return $query->row_array();

	}

	public function archive_utilisateur($id){

		$this->db->where('id',$id);
		$this->db->delete('utilisateur');

	}

	public function activer_utilisateur($id){

		$query = $this->db->get_where('utilisateur', array('id' => $id));

		$utilisateur = $query->row();

		$actif;

		if($utilisateur->actif){
			$actif = 0;
		}else{
			$actif = 1;
		}

		$this->db->where('id',$id);
		$this->db->update('utilisateur',array('actif' => $actif));

	}

}
