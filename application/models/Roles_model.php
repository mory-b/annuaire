<?php

class Roles_model extends CI_Model{

	public function __construct()
	{
		$this->load->database();
	}

	public function get_roles()
	{

		$query = $this->db->get('roles');
		return $query->result_array();

	}

	public function fetch_roles($page, $nbItemsParPage, $archive, $libelle, $trie_libelle)
	{
		
		$query;
		$queryNbItems;

		$this->db->select('*');
		$this->db->from('roles');

		if($archive == 1){
			$this->db->where(array('archive' => 0));
		}
		if($libelle){
			$this->db->like('libelle',$libelle);
		}

		if($trie_libelle == 1){
			$this->db->order_by('libelle', 'ASC');
		}else{
			$this->db->order_by('libelle', 'DESC');
		}
		
		$this->db->limit($nbItemsParPage, $page);
		$query = $this->db->get();

		//----------------------------------
		$this->db->select('*');
		$this->db->from('roles');

		if($archive == 1){
			$this->db->where(array('archive' => 0));
		}
		if($libelle){
			$this->db->like('libelle',$libelle);
		}

		$queryNbItems = $this->db->get();

		return array($query->result_array(),$queryNbItems->num_rows());

	}

	public function get_role($id)
	{

		$query = $this->db->get_where('roles', array('id' => $id));
		return $query->row_array();

	}

	public function insert_role($libelle)
	{

		$data = array(
			'libelle' => $libelle,
			'archive' => 0
		);

		$this->db->insert('roles', $data);

	}

	public function update_role($id,$libelle)
	{
		$query = $this->db->get_where('roles', array('id' => $id));

		$role = $query->row();

		$data = array(
			'libelle' => $libelle,
			'archive' => $role->archive
		);

		$this->db->where('id', $id);
		$this->db->update('roles', $data);

	}

	public function archive_role($id)
	{

		$query = $this->db->get_where('roles', array('id' => $id));

		$role = $query->row();

		$archive;

		if($role->archive){
			$archive = 0;
		}else{
			$archive = 1;
		}

		$this->db->where('id',$id);
		$this->db->update('roles',array('archive' => $archive));

	}

}
