<?php

class Societe_model extends CI_Model{

	public function __construct()
	{

		$this->load->database();

	}

	public function get_societe()
	{

		$query = $this->db->get('societe');
		return $query->result_array();

	}

	public function get_employeur($id)
	{

		$query = $this->db->get_where('societe', array('id' => $id));
		return $query->row_array();

	}
}