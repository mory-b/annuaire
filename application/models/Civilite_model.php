<?php

class Civilite_model extends CI_Model{

	public function __construct()
	{

		$this->load->database();

	}

	public function get_civilite()
	{

		$query = $this->db->get('civilite');
		return $query->result_array();

	}

	public function get_la_civilite($id)
	{

		$query = $this->db->get_where('civilite', array('id' => $id));
		return $query->row_array();

	}
}