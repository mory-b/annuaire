<?php

class Connexion_model extends CI_Model{

	public function __construct()
	{
		$this->load->database();
	}

	public function connexion($email, $mdp)
	{

		$query = $this->db->select('*')
                      ->from('utilisateur')
                      ->where('email', $this->input->post('email'))
                      ->limit(1)
                      ->get();

	    
	    if($query->num_rows() == 1){
	    	$utilisateur = $query->row();

	    	if(password_verify($mdp, $utilisateur->mdp)){
	    		
	    		if($utilisateur->actif == 0){
	      			return array(false,"votre compte n'est plus actif");
	      		}

	      		return array(true,"success de la connexion");
	    	}

	      	return array(false,"email ou mot de passe invalide");
	      	
	    }else{
	      return array(false,"email ou mot de passe invalide");
	    }

	}

  	public function get_utilisateur($email){

		$query = $this->db->get_where('utilisateur', array('email' => $email));
		return $query->row_array();

	}

}
