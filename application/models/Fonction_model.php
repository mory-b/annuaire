<?php

class Fonction_model extends CI_Model{

	public function __construct()
	{

		$this->load->database();

	}

	public function get_fonction()
	{

		$query = $this->db->get('fonctions');
		return $query->result_array();

	}

	public function get_la_fonction($id)
	{

		$query = $this->db->get_where('fonctions', array('id' => $id));
		return $query->row_array();

	}
}