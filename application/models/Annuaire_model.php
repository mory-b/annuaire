<?php

class Annuaire_model extends CI_Model{

	public function __construct(){
		$this->load->database();
	}

	public function get_annuaire(){
			$query = $this->db->get('contact');
			return $query->result_array();
	}

	public function fetch_contacts($page, $nbItemsParPage, $archive, $telephone, $prenom, $nom,$trie_telephone, $trie_prenom, $trie_nom, $trie_societe)
	{
		$query;
		$queryNbItems;
		$parametre=array();

		$this->db->select('c.id, c.telephone_fixe, c.prenom, c.archive, c.nom, s.libelle');
		$this->db->from('contact c');
		$this->db->join('societe s', 's.id = c.employeur','inner');
		if($archive == 1){
			$this->db->where(array('c.archive' => 0));
		}
		if($telephone){
			$this->db->like('c.telephone_fixe',$telephone);
			$this->db->or_like('telephone_mobile',$telephone);
		}
		if($prenom){
			$this->db->like('c.prenom',$prenom);
		}
		if($nom){
			$this->db->like('c.nom',$nom);
		}

		if($trie_nom == 1){
			$this->db->order_by('c.nom', 'ASC');
		}

		if($trie_prenom == 1){
			$this->db->order_by('c.prenom', 'ASC');
		}

		if($trie_telephone == 1){
			$this->db->order_by('c.telephone_fixe', 'ASC');
		}

		if($trie_societe == 1){
			$this->db->order_by('s.libelle', 'ASC');
		}
		
		$this->db->limit($nbItemsParPage, $page);
		$query = $this->db->get();
		

		//--------------------------//
		$this->db->select('c.id, c.telephone_fixe, c.prenom, c.archive, c.nom, s.libelle');
		$this->db->from('contact c');
		$this->db->join('societe s', 's.id = c.employeur','inner');
		if($archive == 1){
			$this->db->where(array('c.archive' => 0));
		}
		if($telephone){
			$this->db->like('c.telephone_fixe',$telephone);
			$this->db->or_like('telephone_mobile',$telephone);
		}
		if($prenom){
			$this->db->like('c.prenom',$prenom);
		}
		if($nom){
			$this->db->like('c.nom',$nom);
		}
		$queryNbItems = $this->db->get();

		
		return array($query->result_array(),$queryNbItems->num_rows());

	}

	public function insert_contact($civilite,$nom,$prenom,$telephone_fixe,$telephone_mobile,$ddn,$email,$employeur,$fonction,$commentaire)
	{

		$query = $this->db->get_where('societe', array('libelle' => $employeur));

		$l_employeur = "";

		if($query->num_rows() > 0){

			$l_employeur = $query->row()->id;

		}else{

			$data_societe = array(
				'libelle' => $employeur
			);

			$this->db->insert('societe', $data_societe);

			$l_employeur =(int) $this->db->insert_id();

		}

		$date = $ddn;

		if(!empty($ddn)){
			$date = DateTime::createFromFormat('d/m/Y', $ddn);
			$date = $date->format('Y-m-d');
		}

		$data_contact = array(
				'civilite' => $civilite,
				'nom' => $nom,
				'prenom' => $prenom,
				'telephone_fixe' => $telephone_fixe,
				'telephone_mobile' => $telephone_mobile,
				'ddn' => $date,
				'email' => $email,
				'employeur' => $l_employeur,
				'fonction' => $fonction,
				//'lien_photo' => $lien_photo,
				'commentaire' => $commentaire,
				'archive' => 0
		);

		$this->db->insert('contact', $data_contact);

	}

	public function update_contact($id, $civilite, $nom, $prenom, $telephone_fixe,$telephone_mobile, $ddn, $email, $employeur, $fonction, $commentaire){

		$query = $this->db->get_where('societe', array('libelle' => $employeur));

		$l_employeur = "";

		if($query->num_rows() > 0){

			$l_employeur = $query->row()->id;

		}else{

			$data_societe = array(
				'libelle' => $employeur
			);

			$this->db->insert('societe', $data_societe);

			$l_employeur = $this->db->insert_id();

		}

		if(!empty($ddn)){
			$date = DateTime::createFromFormat('d/m/Y', $ddn);
			$date = $date->format('Y-m-d');
		}
		
		$data_contact = array(
				'civilite' => $civilite,
				'nom' => $nom,
				'prenom' => $prenom,
				'telephone_fixe' => $telephone_fixe,
				'telephone_mobile' => $telephone_mobile,
				'ddn' => $date,
				'email' => $email,
				'employeur' => $l_employeur,
				'fonction' => $fonction,
				'commentaire' => $commentaire
		);

		$this->db->where('id', $id);
		$this->db->update('contact', $data_contact);

	}

	public function get_contact($id){

		$query = $this->db->get_where('contact', array('id' => $id));
		return $query->row_array();

	}

	public function archive_contact($id){

		$query = $this->db->get_where('contact', array('id' => $id));

		$contact = $query->row();

		$archive;

		if($contact->archive){
			$archive = 0;
		}else{
			$archive = 1;
		}

		$this->db->where('id',$id);
		$this->db->update('contact',array('archive' => $archive));


	}


}
