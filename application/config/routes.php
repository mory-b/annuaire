<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'annuaire';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//route annuaire

$route['annuaire'] = 'annuaire';
$route['annuaire/creation'] = 'annuaire/creation';
$route['annuaire/modification/(:num)']= 'annuaire/modification/$1';
$route['annuaire/detail/(:num)']['GET'] = 'annuaire/detail/$1';
$route['annuaire/archiver/(:num)'] = 'annuaire/archiver/$1';
$route['annuaire/filtrer/(:any)/(:any)/(:any)/(:any)']['GET'] = 'annuaire/filtrer/$1/$2/$3/$4';

//route utilisateur

$route['utilisateur'] = 'utilisateur';
$route['utilisateur/creation'] = 'utilisateur/creation';
$route['utilisateur/modification/(:num)'] = 'utilisateur/modification/$1';
$route['utilisateur/detail/(:num)']['GET'] = 'utilisateur/detail/$1';
$route['utilisateur/archiver/(:num)'] = 'utilisateur/archiver/$1';
$route['utilisateur/filtrer/(:any)/(:any)/(:any)/(:any)']['GET'] = 'utilisateur/filtrer/$1/$2/$3/$4';
$route['utilisateur/activer/(:num)'] = 'utilisateur/activer/$1';

//route roles

$route['fonction'] = 'roles';
$route['fonction/creation'] = 'roles/creation';
$route['fonction/modification/(:num)']= 'roles/modification/$1';
$route['fonction/archiver/(:num)'] = 'roles/archiver/$1';
$route['fonction/filtrer/(:num)/(:num)/(:any)']['GET'] = 'roles/filtrer/$1/$2/$3';
