<?php

class Roles extends CI_Controller{

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url','form','form_validation');
		$this->load->model('roles_model');
		$this->load->library(array('session', 'form_validation','pagination'));
		if(!$this->session->userdata('id')){
			redirect('connexion');
		}elseif ($this->session->userdata('role') != "administrateur") {
			redirect('annuaire');
		}
	}

	public function index()
	{
		$data['identite'] = $this->session->userdata('nom'). ' '. $this->session->userdata('prenom');
		$data['role'] = $this->session->userdata('role');

		$this->load->view('templates/header.php',$data);
		$this->load->view('fonction/index_view');
		$this->load->view('templates/footer.php');
	}

	public function recuperationListeRole($page = 0)
	{

		$nbItemsParPage = 10;

		if($page < 1){
			$page = 0;
		}else{
			$page = ($page - 1) * $nbItemsParPage;
		}

		$roles = $this->roles_model->fetch_roles($page, $nbItemsParPage, $this->input->get('archive'), $this->input->get('libelle'),$this->input->get('trie_libelle'));


		$config['base_url'] = site_url().'Roles/recuperationListeRole';
		$config['total_rows'] = $roles[1];
		$config['use_page_numbers'] = TRUE;
		$config['per_page'] = $nbItemsParPage;

		$this->pagination->initialize($config);

		 $data['pagination'] = $this->pagination->create_links();
		 $data['items'] = $roles[0];
		 $data['nb_items'] = $roles[1];
		 $data['page'] = $page;
		 

		echo json_encode($data);

	}

	public function creation()
	{
		
    	$this->form_validation->set_rules('libelle','libelle', array('required', 'min_length[2]', 'max_length[255]', 'trim', 'is_unique[roles.libelle]'),
    		array(
				'required' => ' Vous devez fournir un %s', 
				'max_length' => 'Le %s doit contenir au plus 255 caractère',
				'min_length' => 'Le %s doit contenir au moins 2 caractère',
				'is_unique' => 'Ce %s existe déjà'
			)
    	);

		$data['title'] = 'ajouter';
		$data['identite'] = $this->session->userdata('nom'). ' '. $this->session->userdata('prenom');
		$data['role'] = $this->session->userdata('role');

		if ($this->form_validation->run() === FALSE){

			$this->load->view('templates/header.php',$data);
			$this->load->view('fonction/formulaire_view',$data);
			$this->load->view('templates/footer.php');

		}else{

			$this->roles_model->insert_role(htmlspecialchars($this->input->post('libelle')));
			redirect('/fonction');

		}

	}

	public function modification($id)
	{

		$data['le_role'] = $this->roles_model->get_role($id);
		$data['identite'] = $this->session->userdata('nom'). ' '. $this->session->userdata('prenom');
		$data['role'] = $this->session->userdata('role');
		$data['title'] = 'modifier';
		$data['id'] = $id;
		$is_unique = '';

    	$array_critere = array('required', 'min_length[2]', 'max_length[255]', 'trim');
    	$array_erreur_message = array(
					'required' => ' Vous devez fournir un %s', 
					'max_length' => 'Le %s doit contenir au plus 255 caractère',
					'min_length' => 'Le %s doit contenir au moins 2 caractère'
				);

    	if($data['le_role']['libelle'] != $this->input->post('libelle')){
	    	array_push($array_critere, 'is_unique[roles.libelle]');
	    	$array_erreur_message['is_unique'] = 'Ce %s existe déjà';

    	}

    	$this->form_validation->set_rules('libelle','libelle', $array_critere, $array_erreur_message);

		if ($this->form_validation->run() === FALSE){
			$this->load->view('templates/header.php',$data);
			$this->load->view('fonction/formulaire_view',$data);
			$this->load->view('templates/footer.php');
		}else{
			if($data['le_role']['libelle'] == $this->input->post('libelle')){
				redirect('/fonction');
			}else{
				$this->roles_model->update_role($id, htmlspecialchars($this->input->post('libelle')));
				redirect('/fonction');
			}
		}
	}

	public function archiver($id)
	{

		$this->roles_model->archive_role($id);
	}

}
