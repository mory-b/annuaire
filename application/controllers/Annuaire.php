<?php

class Annuaire extends CI_Controller{


	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url', 'form');
		$this->load->model('annuaire_model');
		$this->load->model('fonction_model');
		$this->load->model('societe_model');
		$this->load->model('civilite_model');
		$this->load->library(array('session', 'form_validation','pagination'));
		if(!$this->session->userdata('id')){
			redirect('connexion');
		}

	}

	public function index()
	{
		$data['identite'] = $this->session->userdata('nom'). ' '. $this->session->userdata('prenom');
		$data['role'] = $this->session->userdata('role');

		if ($this->session->userdata('role') != "administrateur") {
			$this->load->view('templates/header_utilisateur.php',$data);
		}else{
			$this->load->view('templates/header.php',$data);
		}

		$this->load->view('annuaire/index_view',$data);
		$this->load->view('templates/footer.php');
	}

	public function recuperationListeContact($page = 0)
	{

		$nbItemsParPage = 10;

		if($page < 1){
			$page = 0;
		}else{
			$page = ($page - 1) * $nbItemsParPage;
		}

		$contact = $this->annuaire_model->fetch_contacts($page, $nbItemsParPage, $this->input->get('archive'), $this->input->get('telephone'), $this->input->get('prenom'), $this->input->get('nom'),$this->input->get('trie_telephone'), $this->input->get('trie_prenom'), $this->input->get('trie_nom'), $this->input->get('trie_societe'));

		$config['base_url'] = site_url().'Annuaire/recuperationListeContact';
		$config['total_rows'] = $contact[1];
		$config['use_page_numbers'] = TRUE;
		$config['per_page'] = $nbItemsParPage;

		$this->pagination->initialize($config);

		 $data['pagination'] = $this->pagination->create_links();
		 $data['items'] = $contact[0];
		 $data['societes'] = $this->societe_model->get_societe();
		 $data['nb_items'] = $contact[1];
		 $data['page'] = $page;
		 $data['status'] = $this->session->userdata('role');

		echo json_encode($data);

	}

	public function creation()
	{

		if ($this->session->userdata('role') != "administrateur") {
			redirect('annuaire');
		}

		$this->form_validation->set_rules('civilite','civilite', array('required', 'integer', 'trim'),
			array(
				'required' => ' Vous devez fournir une %s', 
				'integer' => 'La %s doit être un nombre'
			));

    	$this->form_validation->set_rules('nom','nom', array('required', 'min_length[2]', 'max_length[255]', 'trim'),
			array(
				'required' => ' Vous devez fournir un %s', 
				'max_length' => 'Le %s doit contenir au plus 255 caractère',
				'min_length' => 'Le %s doit contenir au moins 2 caractère'
			));

    	$this->form_validation->set_rules('prenom','prenom', array('required', 'min_length[2]', 'max_length[255]', 'trim'),
    		array(
				'required' => ' Vous devez fournir un %s', 
				'max_length' => 'Le %s doit contenir au plus 255 caractère',
				'min_length' => 'Le %s doit contenir au moins 2 caractère'
			));

    	$this->form_validation->set_rules('telephone_fixe','telephone fixe', 
    		array( 'exact_length[10]', 'trim','regex_match[/^0(1|2|3|4|5)[0-9]{8}/]'),
			array(
    			'regex_match'=>'le numéro de n\'est pas valide',
				'exact_length' => 'Le %s n\'est pas valide',
			));

		$this->form_validation->set_rules('ddn','date de naissance', 
    		array( 'trim','regex_match[/^([0-2][0-9]|[3][0-1])\/(((0)[1-9])|((1)[0-2]))\/(19|20)[0-9]{2}$/]'),
			array('regex_match'=>'la %s n\'est pas valide')
    	);

    	$this->form_validation->set_rules('telephone_mobile','telephone mobile', 
    		array( 'exact_length[10]', 'trim','regex_match[/^0(6|7)[0-9]{8}/]'),
    		array(
    			'regex_match'=>'le numéro de n\'est pas valide',
				'exact_length' => 'Le %s n\'est pas valide',
			));

    	$this->form_validation->set_rules('email','email', array( 'valid_email', 'trim','is_unique[contact.email]'),
    		array(
				'valid_email' => 'L\'%s n\'est pas valide',
				'is_unique' => 'Cette %s existe déjà'
			));

    	$this->form_validation->set_rules('commentaires','commentaires', array( 'min_length[2]', 'max_length[255]', 'trim'),
    		array(
				'max_length' => 'Les %s doit contenir au plus 255 caractère',
				'min_length' => 'Les %s doit contenir au moins 2 caractère'
			));

    	$this->form_validation->set_rules('employeur','employeur', array( 'required', 'min_length[2]', 'max_length[255]', 'trim'),
    	array(
				'required' => ' Vous devez fournir un %s', 
				'max_length' => 'Le %s doit contenir au plus 255 caractère',
				'min_length' => 'Le %s doit contenir au moins 2 caractère'
			)
    	);

    	$this->form_validation->set_rules('fonction','fonction', array('required', 'integer', 'trim'),
			array(
				'required' => ' Vous devez fournir une %s', 
				'integer' => 'La %s doit être un nombre'
			));

		$data['identite'] = $this->session->userdata('nom'). ' '. $this->session->userdata('prenom');
		$data['role'] = $this->session->userdata('role');
		$data['title'] = 'ajouter';
		$data['fonctions'] = $this->fonction_model->get_fonction();
		$data['civilites'] = $this->civilite_model->get_civilite();

		if ($this->form_validation->run() === FALSE){
			$this->load->view('templates/header.php',$data);
			$this->load->view('annuaire/formulaire_view',$data);
			$this->load->view('templates/footer.php');
		}else{

			$this->annuaire_model->insert_contact(htmlspecialchars($this->input->post('civilite')),htmlspecialchars($this->input->post('nom')), htmlspecialchars($this->input->post('prenom')), htmlspecialchars($this->input->post('telephone_fixe')), htmlspecialchars($this->input->post('telephone_mobile')), htmlspecialchars($this->input->post('ddn')), htmlspecialchars($this->input->post('email')), htmlspecialchars($this->input->post('employeur')),htmlspecialchars($this->input->post('fonction')), htmlspecialchars($this->input->post('commentaires')));
			 redirect('/annuaire');
	

		}

	}

	public function modification($id)
	{
		if ($this->session->userdata('role') != "administrateur") {
			redirect('annuaire');
		}

    	$data['title'] = 'modifier';
		$data['fonctions'] = $this->fonction_model->get_fonction();
		$data['civilites'] = $this->civilite_model->get_civilite();
		$data['contact'] = $this->annuaire_model->get_contact($id);
		$data['identite'] = $this->session->userdata('nom'). ' '. $this->session->userdata('prenom');
		$data['role'] = $this->session->userdata('role');
		$data['id'] = $id;
		$data['societe'] = $this->societe_model->get_employeur($data['contact']['employeur']);
		
    	$array_critere_mail = array( 'valid_email', 'trim');
    	$array_erreur_message_mail = array('valid_email' => 'L\'%s n\'est pas valide');

    	if($data['contact']['email'] != $this->input->post('email')){
	    	array_push($array_critere_mail, 'is_unique[contact.email]');
	    	$array_erreur_message_mail['is_unique'] = 'Cette %s existe déjà';

    	}

    	$this->form_validation->set_rules('civilite','civilite', array('required', 'integer', 'trim'),
			array(
				'required' => ' Vous devez fournir une %s', 
				'integer' => 'La %s doit être un nombre'
			));

    	$this->form_validation->set_rules('nom','nom', array('required', 'min_length[2]', 'max_length[255]', 'trim'),
			array(
				'required' => ' Vous devez fournir un %s', 
				'max_length' => 'Le %s doit contenir au plus 255 caractère',
				'min_length' => 'Le %s doit contenir au moins 2 caractère'
			));
    	$this->form_validation->set_rules('prenom','prenom', array('required', 'min_length[2]', 'max_length[255]', 'trim'),
    		array(
				'required' => ' Vous devez fournir un %s', 
				'max_length' => 'Le %s doit contenir au plus 255 caractère',
				'min_length' => 'Le %s doit contenir au moins 2 caractère'
			));
    	$this->form_validation->set_rules('telephone_fixe','telephone fixe', 
    		array( 'exact_length[10]', 'trim','regex_match[/^0(1|2|3|4|5)[0-9]{8}/]'),
			array(
    			'regex_match'=>'le numéro de %s n\'est pas valide',
				'exact_length' => 'Le %s n\'est pas valide',
			)
    	);

    	$this->form_validation->set_rules('ddn','date de naissance', 
    		array( 'trim','regex_match[/^([0-2][0-9]|[3][0-1])\/(((0)[1-9])|((1)[0-2]))\/(19|20)[0-9]{2}$/]'),
			array('regex_match'=>'la %s n\'est pas valide')
    	);

    	$this->form_validation->set_rules('telephone_mobile','telephone mobile', 
    		array( 'exact_length[10]', 'trim','regex_match[/^0(6|7)[0-9]{8}/]'),
    		array(
    			'regex_match'=>'le numéro de %s n\'est pas valide',
				'exact_length' => 'Le %s n\'est pas valide',
			)
    	);
    	$this->form_validation->set_rules('email','email', $array_critere_mail,$array_erreur_message_mail);

    	$this->form_validation->set_rules('commentaires','commentaires', array( 'min_length[2]', 'max_length[255]', 'trim'),
    		array(
				'max_length' => 'Le %s doit contenir au plus 255 caractère',
				'min_length' => 'Le %s doit contenir au moins 2 caractère'
			)
    	);
    	$this->form_validation->set_rules('employeur','employeur', array( 'required', 'min_length[2]', 'max_length[255]', 'trim'),
    	array(
				'required' => ' Vous devez fournir un %s', 
				'max_length' => 'Le %s doit contenir au plus 255 caractère',
				'min_length' => 'Le %s doit contenir au moins 2 caractère'
			)
    	);

    	$this->form_validation->set_rules('fonction','fonction', array('required', 'integer', 'trim'),
			array(
				'required' => ' Vous devez fournir une %s', 
				'integer' => 'La %s doit être un nombre'
			));


		if ($this->form_validation->run() === FALSE){
			$this->load->view('templates/header.php',$data);
			$this->load->view('annuaire/formulaire_view',$data);
			$this->load->view('templates/footer.php');
		}else{
			$this->annuaire_model->update_contact($id,htmlspecialchars($this->input->post('civilite')),htmlspecialchars($this->input->post('nom')), htmlspecialchars($this->input->post('prenom')), htmlspecialchars($this->input->post('telephone_fixe')), htmlspecialchars($this->input->post('telephone_mobile')), htmlspecialchars($this->input->post('ddn')), htmlspecialchars($this->input->post('email')), htmlspecialchars($this->input->post('employeur')),htmlspecialchars($this->input->post('fonction')), htmlspecialchars($this->input->post('commentaires')));
			redirect('/annuaire');
		}
	}

	public function detail($id)
	{
		if ($this->session->userdata('role') != "administrateur") {
			redirect('annuaire');
		}
		$data['contact'] = $this->annuaire_model->get_contact($id);
		$data['civilite'] = $this->civilite_model->get_la_civilite($data['contact']['civilite']);
		$data['societe'] = $this->societe_model->get_employeur($data['contact']['employeur']);
		$data['fonction'] = $this->fonction_model->get_fonction();
		$data['identite'] = $this->session->userdata('nom'). ' '. $this->session->userdata('prenom');
		$data['role'] = $this->session->userdata('role');
		$data['fonction'] = $this->fonction_model->get_la_fonction($data['contact']['fonction']);
		$data['id'] = $id;

		$this->load->view('templates/header.php',$data);
		$this->load->view('annuaire/detail_view',$data);
		$this->load->view('templates/footer.php');

	}

	public function archiver($id)
	{
		$this->annuaire_model->archive_contact($id);
	}


}
