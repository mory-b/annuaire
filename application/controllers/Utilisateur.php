<?php

class Utilisateur extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url','form');
		$this->load->library(array('session', 'form_validation','pagination'));
		$this->load->model('utilisateur_model');
		$this->load->model('roles_model');
		if(!$this->session->userdata('id')){
			redirect('connexion');
		}elseif ($this->session->userdata('role') != "administrateur") {
			redirect('annuaire');
		}
	}

	public function index()
	{

		$data['identite'] = $this->session->userdata('nom'). ' '. $this->session->userdata('prenom');
		$data['role'] = $this->session->userdata('role');
		
		$role_array = array();

		foreach ($this->roles_model->get_roles() as $key => $value) {

			if($value['archive'] == 0){
				$role_array[$value['id']] = $value['libelle'];
			}

		}

		$data['roles'] = $role_array;

		$this->load->view('templates/header.php',$data);
		$this->load->view('utilisateur/index_view');
		$this->load->view('templates/footer.php');

	}

	public function recuperationListeUtilisateur($page = 0)
	{

		$nbItemsParPage = 10;

		if($page < 1){
			$page = 0;
		}else{
			$page = ($page - 1) * $nbItemsParPage;
		}

		$roles = $this->utilisateur_model->fetch_utilisateur($page, $nbItemsParPage, $this->input->get('actif'), $this->input->get('role'), $this->input->get('nom'), $this->input->get('prenom'), $this->input->get('trie_nom'), $this->input->get('trie_prenom'),$this->input->get('trie_role'));



		$config['base_url'] = site_url().'Utilisateur/recuperationListeUtilisateur';
		$config['total_rows'] = $roles[1];
		$config['use_page_numbers'] = TRUE;
		$config['per_page'] = $nbItemsParPage;

		$this->pagination->initialize($config);

		 $data['pagination'] = $this->pagination->create_links();
		 $data['items'] = $roles[0];
		 $data['nb_items'] = $roles[1];
		 $data['page'] = $page;

		echo json_encode($data);

	}

	public function creation(){

		$this->form_validation->set_rules('nom','nom', array('required', 'max_length[255]', 'trim'),
			array(
				'required' => ' Vous devez fournir un %s', 
				'max_length' => 'Le %s doit contenir au plus 255 caractère'
			)
		);

		$this->form_validation->set_rules('prenom','prenom', array('required', 'max_length[255]', 'trim'),
			array(
				'required' => ' Vous devez fournir un %s', 
				'max_length' => 'Le %s doit contenir au plus 255 caractère'
			)
		);

		$this->form_validation->set_rules('identifiant','identifiant', array( 'required', 'min_length[2]', 'max_length[255]', 'trim'),
			array(
				'required' => ' Vous devez fournir un %s', 
				'min_length' => 'Le %s doit contenir au moins 2 caractère',
				'max_length' => 'Le %s doit contenir au plus 255 caractère'
			)
		);

		$this->form_validation->set_rules('mdp','mot de passe', array( 'required', 'min_length[8]', 'max_length[255]', 'trim'), 
			array(
				'required' => ' Vous devez fournir un %s', 
				'min_length' => 'Le %s doit contenir au moins 8 caractère',
				'max_length' => 'Le %s doit contenir au plus 255 caractère'
			)
		);

		$this->form_validation->set_rules('email','email', array( 'required', 'valid_email', 'trim','is_unique[utilisateur.email]'),
			array('required' => ' Vous devez fournir un %s','is_unique' => 'Cette %s existe déjà') 
		);

		$data['title'] = 'ajouter';
		$data['roles'] = $this->roles_model->get_roles();
		$data['identite'] = $this->session->userdata('nom'). ' '. $this->session->userdata('prenom');
		$data['role'] = $this->session->userdata('role');

		if ($this->form_validation->run() === FALSE){

			$this->load->view('templates/header.php',$data);
			$this->load->view('utilisateur/formulaire_view',$data);
			$this->load->view('templates/footer.php');

		}else{

			$mdp_hash = password_hash($this->input->post('mdp'), PASSWORD_BCRYPT);
			$resultat = $this->utilisateur_model->insert_utilisateur(htmlspecialchars($this->input->post('identifiant')), htmlspecialchars($this->input->post('nom')), htmlspecialchars($this->input->post('prenom')), htmlspecialchars($this->input->post('role')), htmlspecialchars($this->input->post('email')), $mdp_hash);
			
			if($resultat == false){
				$data['erreur_identifiant'] = "identifiant déjà utilisé";

				$this->load->view('templates/header.php',$data);
				$this->load->view('utilisateur/formulaire_view',$data);
				$this->load->view('templates/footer.php');
			}else{
				redirect('/utilisateur');
			}

		}
	}

	public function modification($id){

		$data['title'] = 'modifier';
		$data['roles'] = $this->roles_model->get_roles();
		$data['identite'] = $this->session->userdata('nom'). ' '. $this->session->userdata('prenom');
		$data['role'] = $this->session->userdata('role');
		$data['utilisateur'] = $this->utilisateur_model->get_utilisateur($id);
		$data['id'] = $id;

		$array_critere_mail = array( 'required', 'valid_email', 'trim');
    	$array_erreur_message_mail = array('valid_email' => 'L\'%s n\'est pas valide');

    	if($data['utilisateur']['email'] != $this->input->post('email')){
	    	array_push($array_critere_mail, 'is_unique[utilisateur.email]');
	    	$array_erreur_message_mail['is_unique'] = 'Cette %s existe déjà';

    	}

		$this->form_validation->set_rules('nom','nom', array('required', 'max_length[255]', 'trim'),
			array(
				'required' => ' Vous devez fournir un %s', 
				'max_length' => 'Le %s doit contenir au plus 255 caractère'
			)
		);

		$this->form_validation->set_rules('prenom','prenom', array('required', 'max_length[255]', 'trim'),
			array(
				'required' => ' Vous devez fournir un %s', 
				'max_length' => 'Le %s doit contenir au plus 255 caractère'
			)
		);

		$this->form_validation->set_rules('email','email', $array_critere_mail, $array_erreur_message_mail);

		$this->form_validation->set_rules('mdp','mot de passe', array( 'min_length[8]', 'max_length[255]', 'trim'), 
			array( 
				'min_length' => 'Le %s doit contenir au moins 8 caractère',
				'max_length' => 'Le %s doit contenir au plus 255 caractère'
			)
		);

		if ($this->form_validation->run() === FALSE){
			$this->load->view('templates/header.php',$data);
			$this->load->view('utilisateur/formulaire_view',$data);
			$this->load->view('templates/footer.php');
		}else{

			if($this->input->post('mdp') == ''){
				$this->utilisateur_model->update_utilisateur($id, htmlspecialchars($this->input->post('nom')), htmlspecialchars($this->input->post('prenom')), htmlspecialchars($this->input->post('role')), htmlspecialchars($this->input->post('email')), htmlspecialchars($this->input->post('mdp')));
			}else{
				$mdp_hash = password_hash($this->input->post('mdp'), PASSWORD_BCRYPT);
				$this->utilisateur_model->update_utilisateur($id, htmlspecialchars($this->input->post('nom')), htmlspecialchars($this->input->post('prenom')), htmlspecialchars($this->input->post('role')), htmlspecialchars($this->input->post('email')), $mdp_hash);
			}

			redirect('/utilisateur');
			
		}
	}

	public function activer($id){
		$this->utilisateur_model->activer_utilisateur($id);
	}

	public function archiver($id){
		$this->utilisateur_model->archive_utilisateur($id);
	}

}
