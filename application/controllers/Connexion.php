<?php

class connexion extends CI_Controller{

	public function __construct()
	{
		parent::__construct();

		$this->load->helper(array('url','form'));
		$this->load->library(array('session', 'form_validation'));
		$this->load->model('connexion_model');
		$this->load->model('utilisateur_model');
		$this->load->model('roles_model');
		
	}

	public function index()
	{
			$this->form_validation->set_rules('mdp','mdp', array( 'required', 'trim'));
			$this->form_validation->set_rules('email','email', array( 'valid_email', 'trim'));


			if ($this->form_validation->run() === FALSE){
			 	$this->load->view('connexion/connexion_view');
			}else{
				$connexion_valide = $this->connexion_model->connexion($this->input->post('email'), $this->input->post('mdp'));

				if($connexion_valide[0] == true){

						$utilisateur = $this->connexion_model->get_utilisateur($this->input->post('email'));
						$utilisateur_role = $this->roles_model->get_role($utilisateur['role']);

						$newdata = array(
					        'prenom'  => $utilisateur['prenom'],
							'nom'  => $utilisateur['nom'],
					        'id'     => $utilisateur['id'],
							'role' => $utilisateur_role['libelle']
					);

					$this->session->set_userdata($newdata);
					redirect('annuaire');
				}else{
					$data['message_erreur']=$connexion_valide[1];
					$this->load->view('connexion/connexion_view',$data);
				}
				
			}

	}

	public function deconnexion()
	{
			$this->session->unset_userdata(array('prenom','nom','id','role'));
		 	redirect('/connexion');
	}
}
